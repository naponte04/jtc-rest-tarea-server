package py.com.jtc.rest.repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import py.com.jtc.rest.bean.Empleado;

@Repository
public class EmpleadoRepositoryImpl implements EmpleadoRepository{

	@Autowired
	private JdbcTemplate jdbcTemplate;
		
	@Override
	public List<Empleado> obtenerEmpleados() {
		List<Empleado> empleados = jdbcTemplate.query("select * from empleados", 
				(rs) -> {
					List<Empleado> list = new ArrayList<>();
					while(rs.next()){
						Empleado c = new Empleado();
						c.setId(rs.getInt("id"));
						c.setNombre(rs.getString("nombre"));
						c.setEdad(rs.getInt("edad"));
						c.setSalario(rs.getLong("salario"));
						list.add(c);
					}
					return list;
				});
		return empleados;
	}
	
	@Override
	public Empleado obtenerEmpleado(Integer id) {
		Empleado empleado = jdbcTemplate.queryForObject("select * from empleados where id = ?", 
				new Object[] {id}, 
				(rs, rowNum) -> {
					Empleado c = new Empleado();
					c.setId(rs.getInt("id"));
					c.setNombre(rs.getString("nombre"));
					c.setEdad(rs.getInt("edad"));
					c.setSalario(rs.getLong("salario"));
					return c;
		        });
		
		return empleado;
	}

	@Override
	public Empleado agregarEmpleado(Empleado empleado) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		PreparedStatementCreator psc = (conn) -> {
			PreparedStatement ps = conn.prepareStatement("insert into empleados values (?,?,?,?)", 
					Statement.RETURN_GENERATED_KEYS);
			
			ps.setNull(1, Types.INTEGER);
			ps.setString(2, empleado.getNombre());
			ps.setInt(3,  empleado.getEdad());
			ps.setLong(4, empleado.getSalario());
	        return ps;
		}; 
		
		int result = jdbcTemplate.update(psc, keyHolder);
		
		if (result > 0)
			empleado.setId(keyHolder.getKey().intValue());
		
		return empleado;
		
	}

	@Override
	public boolean eliminarEmpleado(Integer id) {
		int result = jdbcTemplate.update("delete from empleados where id = ?", id);
		
		return (result > 0) ? true : false;
	}

	@Override
	public boolean actualizarEmpleado(Empleado empleado, Integer id) {
		int result = jdbcTemplate.update("update empleados set nombre = ?, edad = ?, salario = ? where id = ?", 
				empleado.getNombre(), empleado.getEdad(), empleado.getSalario(), id);
		return (result > 0) ? true : false;
	}

}
