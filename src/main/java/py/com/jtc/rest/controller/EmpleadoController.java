package py.com.jtc.rest.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.jtc.rest.bean.Empleado;
import py.com.jtc.rest.repository.EmpleadoRepository;

@RestController
@RequestMapping("/empleados")
public class EmpleadoController {

	@Autowired
	private EmpleadoRepository empleadoRepository;
	
	@GetMapping
	public List<Empleado> obtenerEmpleados() {
		//TODO: pendiente
	}
	
	@GetMapping("/{id}")
	public Empleado obtenerEmpleado(@PathVariable("id") String id) {
		//TODO: pendiente
	}
	
	@PostMapping
	public ResponseEntity<Empleado> guardarEmpleado(@RequestBody Empleado empleado, UriComponentsBuilder uBuilder) {
		Empleado c = empleadoRepository.agregarEmpleado(empleado);
		HttpHeaders header = new HttpHeaders();
		
		URI uri = uBuilder.path("/empleados/" )
				.path(String.valueOf(empleado.getId()))
				.build()
				.toUri();
		
		header.setLocation(uri);
		
		return new ResponseEntity<>(c, header, HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	public Empleado editarEmpleado(@PathVariable("id") String id,
			@RequestBody Empleado empleado, 
			UriComponentsBuilder uBuilder) 
	{
		Empleado c = null;
		boolean result = empleadoRepository.actualizarEmpleado(empleado, Integer.valueOf(id));
		if (result) {
			 c = obtenerEmpleado(id);
		}
		return c;
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminarEmpleado(@PathVariable("id") String id) { 
		//TODO: pendiente
	}
}
